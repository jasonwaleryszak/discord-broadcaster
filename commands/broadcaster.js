const { BroadcasterEmbed, Messages } = require('../');

module.exports = {
    name: 'broadcaster',
    description: '...',
    usage: '!broadcaster <create, delete, list>',
    aliases: ['bc'],
    execute(client, message, args) {
        if (this.usage !== '!broadcaster <create, delete, list>')
            this.usage = '!broadcaster <create, delete, list>';

        // check user permission
        if (!message.member.hasPermission('ADMINISTRATOR'))
            return message.channel.send(
                BroadcasterEmbed.denied()
            );

        // check for min of sub-command
        if (args.length < 1) return message.channel.send(
            BroadcasterEmbed.err(`${Messages.INVALID_SUBCOMMAND}, \`${this.usage}\``)
        );

        const subCommand = args[0].trim().toLowerCase();

        switch (subCommand) {
            case 'create':
                this.usage = '!broadcaster create <name> <delay> <message>';

                // remove sub-command from args array
                args.shift();

                if (args.length < 3) return message.channel.send(
                    BroadcasterEmbed.err(`${Messages.INVALID_ARGUMENTS}, \`${this.usage}\``)
                );

                // name of broadcast
                const name = args[0].toLowerCase();

                client.settings.get(message.guild.id, 'broadcasts');

                let check = client.settings.fetch(message.guild.id).broadcasts;
                let hmm = check.filter((b) => b.name === name);

                if (hmm.length !== 0) return message.channel.send(
                    BroadcasterEmbed.err(`${Messages.BROADCAST_NAME_TAKEN}`)
                );

                // remove name from args array
                args.shift();

                // attempts to convert string arg to delay int
                let delay = parseInt(args[0]);

                if (isNaN(delay)) return message.channel.send(
                    BroadcasterEmbed.err(`${Messages.INVALID_DELAY_TYPE}`)
                );

                args.shift();

                // shift and join args
                let msg = args.join();

                let channel = message.channel.id;

                delay *= 60;
                delay *= 1000;

                client.settings.push(message.guild.id,
                    {
                        name : name,
                        message : msg,
                        channel : channel,
                        delay : delay
                    },
                    'broadcasts');

                const interval = setInterval(() => {
                    message.channel.send(`${msg}`);
                }, delay);

                client.intervals.push(name, interval);

                return message.channel.send(
                  BroadcasterEmbed.success(`${Messages.BROADCAST_CREATED.replace('$name', name)}`)
                );

            case 'delete':
                this.usage = '!broadcaster delete <name>';

                // remove sub-command from args array
                args.shift();

                if (args.length !== 1) return message.channel.send(
                    BroadcasterEmbed.err(`${Messages.INVALID_ARGUMENTS}, \`${this.usage}\``)
                );

                const data = client.settings.fetch(message.guild.id).broadcasts;
                const filteredData = data.filter(b => b.name !== args[0]);

                if (data.length === filteredData.length) return message.channel.send(
                    BroadcasterEmbed.err(`${Messages.INVALID_TARGET}`)
                );
                client.settings.set(message.guild.id, filteredData, 'broadcasts');
                const inter = client.intervals.get(args[0]);

                clearTimeout(inner);

                return message.channel.send(BroadcasterEmbed.success(`\`${inter.name}\` has been deleted`));

            case 'list':
                this.usage = '!broadcaster list';

                if (args.length > 1) return message.channel.send(
                    BroadcasterEmbed.err(`${Messages.INVALID_ARGUMENTS}, \`${this.usage}\``)
                );

                const broadcasts = client.settings.fetch(message.guild.id).broadcasts;

                if (!broadcasts.length) return message.channel.send(
                    BroadcasterEmbed.notice(`${Messages.NO_BROADCASTS}`)
                );

                let names = [];

                broadcasts.forEach((b) => {
                    names.push(b.name);
                });

                const embed = new BroadcasterEmbed({
                    description: `Broadcasts for \`${message.guild.name}\`: \n ${names.join(', ')}`
                });
                return message.channel.send(embed);

            default:
                return message.channel.send(
                    BroadcasterEmbed.err(`${Messages.INVALID_SUBCOMMAND}, \`${this.usage}\``)
                );
        }
    }
};
