const Enmap = require('enmap');
const Discord = require('discord.js');
const fs = require('fs');

const client = new Discord.Client();
client.commands = new Discord.Collection();
client.intervals = new Discord.Collection();

const { token } = require('./config.json');

module.exports = {
    BroadcasterEmbed: require('./utils/BroadcasterEmbed'),
    Messages: require('./messages')
};

client.settings = new Enmap({
    name: "settings",
    fetchAll: false,
    autoFetch: true,
    cloneLevel: "deep"
});

client.defaultSettings = {
    prefix: '!',
    broadcasts: []
};

// register commands
fs.readdirSync('./commands')
    .filter(file => file.endsWith('.js'))
    .forEach((file) => {
        const command = require(`./commands/${file}`);
        client.commands.set(command.name, command);
    });

// register commands
fs.readdirSync('./events')
    .filter(file => file.endsWith('.js'))
    .forEach((file) => {
        const event = require(`./events/${file}`);
        const eventName = file.split('.')[0];
        client.on(eventName, event.bind(null, client));
    });

// login with super secret token
client.login(token);
