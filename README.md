# discord-broadcaster

Open source broadcaster bot for Discord built with discord.js

## Installation

### Prerequisites

This bot is built using [discord.js](https://discord.js.org/#/). To run the bot, you will need to install the dependencies and run using the node command. Be sure Node+NPM is installed before beginning.

- [node](https://nodejs.org/en/download/) >= 10.15.3
- [npm](https://nodejs.org/en/download/) >= 6.4.1
- [git](https://git-scm.com/downloads) >= latest recommended

### Repository

Download the repo using `git clone` in your command prompt of choice. 

Run `git clone https://gitlab.com/FLY1NN/discord-broadcaster.git` to download the repo.

### Installing Dependencies

Open the downloaded repo in the command line and run `npm install`. This will install all dependencies required for the bot.

## Configuration

You will need to provide your own token for the bot to run. Go to the [Discord Developer Portal](https://discordapp.com/developers/applications/), login, and create a new application. Name the application. Go to the `Bot` tab and `Add Bot`. Copy the token and replace the dummy token in `config.json`. Go to the `OAuth2` tab in the Developer Portal and `Add Redirect`. Add `https://discordapp.com/oauth2/authorize?&client_id=CLIENT_ID&scope=bot`. You will have to replace `CLIENT_ID` with your bot's id (found on `General Information` tab). Select the new redirect in `Select Redirect URL`. You can use this url to add the bot to a server. 
 
## Running the Bot

Jump back to your terminal opened to the bot source directory. Run `node index` to start the bot. 

## Usage

This bot has only one command: `broadcaster`, alias `bc`. Each command must be preceded by the `prefix`. It is defined in `config.json` and is `!` by default. Below I have provided full usage of the `broadcaster` command:


| broadcaster : bc | | | | |
| ------- | ------ | ------ | ------- | --- |
| !broadcaster | create | \<name\> | \<delay\> | \<message\> |
| !broadcaster | delete | \<name\> |
| !broadcaster | list |

> NOTE: The delay is measured in minutes, it can be modified in `commands/broadcaster.js`.

## Issues?

If you find bugs, create an issue on GitLab. This is the best way for me to be able to manage issues effectively. If you have questions, feel free to reach out on Discord, `json#0001`.
