module.exports = (client, message) => {
    const settings = client.settings.ensure(message.guild.id, client.defaultSettings);

    if (!message.content.startsWith(settings.prefix) || message.author.bot) return;

    const args = message.content.slice(settings.prefix.length).split(/ +/);
    const commandName = args.shift().toLowerCase();

    const command = client.commands.get(commandName) ||
                    client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

    if (!command) return;

    try {
        command.execute(client, message, args);
    } catch (err) {
        console.log(err);
    }
};
